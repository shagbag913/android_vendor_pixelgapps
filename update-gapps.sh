#!/usr/bin/env bash

SYSTEMIMAGE=$1

if [[ -z $SYSTEMIMAGE ]]; then
    echo "You must specify the location of a mounted system image! Aborting"
    exit 1
fi

UPDATE_BLACKLIST="
etc/sysconfig/nexus.xml
priv-app/Android.mk
app/Android.mk
framework/Android.mk
lib64/Android.mk
"

for i in app priv-app framework lib lib64 etc; do
    FILE=$(find $i/ -type f)
    for f in $FILE; do
        OLDMD5=$(md5sum $f)
        if ! echo "$UPDATE_BLACKLIST" | grep "$f" &>/dev/null; then
            if ! cp $SYSTEMIMAGE/$f ./$f 2>/dev/null; then
                echo "! $f not found in system image location"
            else
                NEWMD5=$(md5sum $f)
                if [[ $NEWMD5 != $OLDMD5 ]]; then
                    echo "+ updated $f"
                fi
            fi
        else
            echo "- Not updating $f, in update blacklist"
        fi
    done
done
