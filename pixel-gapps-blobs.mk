# Copyright (C) 2017 The Pure Nexus Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

PRODUCT_COPY_FILES += \
    vendor/pixelgapps/etc/permissions/com.google.vr.platform.xml:system/etc/permissions/com.google.vr.platform.xml \
    vendor/pixelgapps/etc/permissions/com.google.android.dialer.support.xml:system/etc/permissions/com.google.android.dialer.support.xml \
    vendor/pixelgapps/etc/permissions/com.google.android.maps.xml:system/etc/permissions/com.google.android.maps.xml \
    vendor/pixelgapps/etc/permissions/turbo.xml:system/etc/permissions/turbo.xml \
    vendor/pixelgapps/etc/preferred-apps/google.xml:system/etc/preferred-apps/google.xml \
    vendor/pixelgapps/etc/sysconfig/framework-sysconfig.xml:system/etc/sysconfig/framework-sysconfig.xml \
    vendor/pixelgapps/etc/sysconfig/google.xml:system/etc/sysconfig/google.xml \
    vendor/pixelgapps/etc/sysconfig/google_build.xml:system/etc/sysconfig/google_build.xml \
    vendor/pixelgapps/etc/sysconfig/google-hiddenapi-package-whitelist.xml:system/etc/sysconfig/google-hiddenapi-package-whitelist.xml \
    vendor/pixelgapps/etc/sysconfig/google_vr_build.xml:system/etc/sysconfig/google_vr_build.xml \
    vendor/pixelgapps/etc/sysconfig/hiddenapi-package-whitelist.xml:system/etc/sysconfig/hiddenapi-package-whitelist.xml \
    vendor/pixelgapps/etc/sysconfig/nexus.xml:system/etc/sysconfig/nexus.xml \
    vendor/pixelgapps/lib/libdmengine.so:system/lib/libdmengine.so \
    vendor/pixelgapps/lib/libdmjavaplugin.so:system/lib/libdmjavaplugin.so \
    vendor/pixelgapps/lib/libdvr.so:system/lib/libdvr.so \
    vendor/pixelgapps/lib/libdvr_loader.so:system/lib/libdvr_loader.so \
    vendor/pixelgapps/lib/libeaselcomm.so:system/lib/libeaselcomm.so \
    vendor/pixelgapps/lib/libfilterpack_facedetect.so:system/lib/libfilterpack_facedetect.so \
    vendor/pixelgapps/lib/libfrsdk.so:system/lib/libfrsdk.so \
    vendor/pixelgapps/lib/libprotobuf-cpp-full.so:system/lib/libprotobuf-cpp-full.so \
    vendor/pixelgapps/lib/libsketchology_native.so:system/lib/libsketchology_native.so \
    vendor/pixelgapps/lib64/libdvr.so:system/lib64/libdvr.so \
    vendor/pixelgapps/lib64/libdvr_loader.so:system/lib64/libdvr_loader.so \
    vendor/pixelgapps/lib64/libeaselcomm.so:system/lib64/libeaselcomm.so \
    vendor/pixelgapps/lib64/libfacenet.so:system/lib64/libfacenet.so \
    vendor/pixelgapps/lib64/libfilterpack_facedetect.so:system/lib64/libfilterpack_facedetect.so \
    vendor/pixelgapps/lib64/libfrsdk.so:system/lib64/libfrsdk.so \
    vendor/pixelgapps/lib64/libgdx.so:system/lib64/libgdx.so \
    vendor/pixelgapps/lib64/libjni_latinimegoogle.so:system/lib64/libjni_latinimegoogle.so \
    vendor/pixelgapps/lib64/libjni_latinime.so:system/lib64/libjni_latinime.so \
    vendor/pixelgapps/lib64/libsketchology_native.so:system/lib64/libsketchology_native.so \
    vendor/pixelgapps/lib64/libvr_hwc-hal.so:system/lib64/libvr_hwc-hal.so \
    vendor/pixelgapps/app/CalculatorGooglePrebuilt/CalculatorGooglePrebuilt.apk.prof:system/app/CalculatorGooglePrebuilt/CalculatorGooglePrebuilt.apk.prof \
    vendor/pixelgapps/app/Chrome/Chrome.apk.prof:system/app/Chrome/Chrome.apk.prof \
    vendor/pixelgapps/app/GoogleCamera/GoogleCamera.apk.prof:system/app/GoogleCamera/GoogleCamera.apk.prof \
    vendor/pixelgapps/app/GoogleContacts/GoogleContacts.apk.prof:system/app/GoogleContacts/GoogleContacts.apk.prof \
    vendor/pixelgapps/app/PrebuiltDeskClockGoogle/PrebuiltDeskClockGoogle.apk.prof:system/app/PrebuiltDeskClockGoogle/PrebuiltDeskClockGoogle.apk.prof \
    vendor/pixelgapps/app/PrebuiltGmail/PrebuiltGmail.apk.prof:system/app/PrebuiltGmail/PrebuiltGmail.apk.prof
